<?php

namespace hs\rabbitmq;

use think\facade\App;
use think\facade\Cache;
use think\facade\Log;

class TestConsumer
{

    use Service;

    public function jobs(array $param,int $retry)
    {
        $number = Cache::get($param['params']['solevar'],0);
        if($number < 5){
            $number++;
            Cache::set($param['params']['solevar'],$number);
            $this->setExecMsg($param['params']['solevar'],"TestConsumer failed {$number}");
            return false;
        }
        $this->setExecMsg($param['params']['solevar'],"TestConsumer success {$number}");
        return true;
    }
    

}