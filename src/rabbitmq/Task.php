<?php

namespace hs\rabbitmq;

use think\Model;

class Task extends Model
{

    /**
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    // 设置字段信息
    protected $schema = [
        'id'               => 'int',
        'solevar'          => 'string',
        'status'           => 'tinyint',
        'queue_name'       => 'string',
        'body'             => 'text',
        'number'           => 'tinyint',
        'exec_number'      => 'tinyint',
        'next_exec_time'   => 'datetime',
        'exec_class'       => 'string',
        'exec_func'        => 'string',
        'create_time'      => 'datetime',
        'update_time'      => 'datetime',
    ];

    /**
     * 获取任务模型对象
     *
     * @return Task 任务对象
     */
    public static function model(): Task
    {
        return new self();
    }

}