<?php

namespace hs\rabbitmq;

use think\Model;

class TaskExec extends Model
{

    /**
     * @var string
     */
    protected $autoWriteTimestamp = 'datetime';

    // 设置字段信息
    protected $schema = [
        'id'               => 'int',
        'solevar'          => 'string',
        'status'           => 'tinyint',
        'response'         => 'text',
        'exec_start_time'  => 'datetime',
        'exec_end_time'    => 'datetime',
        'exec_number'      => 'tinyint',
        'create_time'      => 'datetime'
    ];

    /**
     * 获取任务模型对象
     *
     * @return Task 任务对象
     */
    public static function model(): TaskExec
    {
        return new self();
    }

}