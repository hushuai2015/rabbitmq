<?php
namespace hs\rabbitmq;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use think\facade\Log;

/**
 * 消费者类
 * Class Consumer
 * @package hs\rabbitmq
 */
class Consumer extends Common
{


    /**
     * @var null
     */
    public $class = null;

    /**
     * @var array
     */
    protected $queueConfig = [];

    public function consumer($name,$delay="")
    {

        //获取配置
        $this->queueConfig = $this->queue[$name];
        $this->queueConfig['key'] = $name;

        //建立通道
        $channel = $this->connection->channel();

        //流量控制
        $channel->basic_qos(null, 1, null);

        if(empty($delay)){
            $type = "topic";
            $args = [];
            $exchange_name = $this->topic['exchange_name'];
        }else{
            $type = "x-delayed-message";
            $args = new AMQPTable([
                'x-delayed-type' => 'direct'
            ]);
            $exchange_name = "delayed_{$this->topic['exchange_name']}";
        }
        //初始化交换机
        $channel->exchange_declare($exchange_name, $type , false, true, false);

        //初始化队列  注释调这句如果队列不存在则会报错
        $channel->queue_declare($this->queueConfig['queue_name'],false, true,false,false,false,$args);

        //绑定队列与交换机
        $channel->queue_bind($this->queueConfig['queue_name'], $exchange_name, $this->queueConfig['route_key']);

        /**
         * 消费消息
         * $queue 消息队列名称
         * $consumer_tag 消费者标签，用来区分多个消费者
         * $no_local 设置为true，表示 不能将同一个Conenction中生产者发送的消息传递给这个Connection中 的消费者
         * $no_ack 是否自动确认消息,true自动确认,false 不自动要手动调用,建立设置为false
         * $exclusive 是否排他
         */
        $channel->basic_consume(
            $this->queueConfig['queue_name'],
            $this->queueConfig['consumer_tag'],
            false,
            $this->queueConfig['no_ack'],
            false,
            false,
            [$this, 'handlerMsg']
        );

        //退出
        register_shutdown_function([$this, 'shutdown'], $channel, $this->connection);

        //监听
        while(count($channel->callbacks)) {
            $channel->wait();
        }

    }

    /**
     * @param $msg
     */
    public function handlerMsg(AMQPMessage $msg)
    {
        try {
            $data['params'] = json_decode($msg->getBody(),true);
            $data['queueConfig'] = $this->queueConfig;
            $this->setQueueMsg($data['params'])->setExecStartTime()->getTask()->setNextExecTime();
            //判断是否达到最大执行次数
            if(!$this->isExecMax()){
                $res = $this->job($data,$msg->getDeliveryTag());
                $this->updateTask($res);
                $msg->ack();
                $this->retry();
            }else{
                $msg->ack();
            }
        }catch (\Exception $exception){  /* 程序出现异常自动消费 */
            Log::error('Comsumer handlerMsg Exception : ' . $exception->getMessage() . ' time: '.date('Y-m-d H:i:s'));
            $this->queueConfig['no_ack'] ?: $msg->ack();
        }


    }

    /**
     * 退出
     * @param  $channel [信道]
     * @param $connection [连接]
     */
    public function shutdown($channel, $connection)
    {
        $channel->close();
        $connection->close();
    }


}
