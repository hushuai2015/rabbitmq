<?php

namespace hs\rabbitmq;

use hs\model\Task;
use think\console\input\Argument;
use think\facade\App;
use think\facade\Cache;
use think\facade\Log;

class ConsumerCommon
{


    public $app;
    public $cahce;
    public $log;

    /**
     * 任务唯一标识
     * @var mixed
     */
    public $solevar;

    public $body;
    public $number;

    public $task;


    public function __construct(array $param=[],App $app,Log $log)
    {
        $this->app = $app;
        $this->log = $log;
        $this->solevar = $param['solevar'];
        $this->body    = $param['body'];
        $this->number  = $param['number'] ?? 0;
    }

    /**
     * 错误日志
     * @param $message
     * @return void
     */
    public function errorLog($message)
    {
        $msg  = "[{$this->app->request->action()}]";
        $msg .= is_string($message) ? $message : var_export($message,true);
        Log::record($msg,"error");
    }


    public function init()
    {

    }

    public function attempts()
    {
    }

    public function release(){

    }

}